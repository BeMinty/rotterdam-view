const gulp = require('gulp'),
      config = require('../config').duplicate.dist.images;

gulp.task('copy:images', () => {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
