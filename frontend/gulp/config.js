const paths = {
  nodeModules: 'node_modules/',
  src: 'frontend/src/',
  dist: 'frontend/dist/',
  assets: 'frontend/src/assets/',
  backend: 'backend/'
}

module.exports = {
  browsersync: {
    watch: true,
    server: paths.dist,
    open: false,
    reloadOnRestart: true,
    notify: true
  },
  scss: {
    src: paths.src + 'components/**/*.{sass,scss}',
    dest: paths.dist + 'css/',
    options: {
      outputStyle: 'expanded',
      errLogToConsole: true
    }
  },
  autoprefixer: {
    browsers: [
      'last 3 versions'
    ],
    cascade: false
  },
  nunjucks: {
    src: paths.src + 'templates/**/**/*.nunjucks',
    dest: paths.dist,
    options: {
      path: [
        paths.src + 'templates/',
        paths.src + 'components/'
      ]
    },
    paths: [
      paths.src + 'templates/**/**/*.nunjucks',
      paths.src + 'components/**/*.html'
    ]
  },
  html: {
    src: paths.dist,
    dest: paths.backend,
    options: {
      indent_size: 2
    }
  },
  babel: {
    src: paths.src + 'scripts/scripts.js',
    dest: paths.dist + 'js/'
  },
  concatenate: {
    js: {
      src: [
        paths.src + 'scripts/vendors/jquery.js',
        paths.nodeModules + 'bootstrap/dist/js/bootstrap.bundle.js',
        paths.nodeModules + 'slick-carousel/slick/slick.js',
        paths.src + 'scripts/scripts.js'
      ],
      filename: 'scripts.js',
      dest: paths.dist + 'js/'
    }
  },
  notification: {
    scss: {
      message: "Sass task complete"
    },
    nunjucks: {
      message: "Nunjucks task complete"
    },
    concat: {
      message: "Concat task complete"
    },
    babel: {
      message: "Babel task complete"
    }
  },
  delete_it: {
    dist: {
      src: [
        paths.dist + 'css/*.css',
        paths.dist + 'js/*.js',
        paths.dist + '**/**/*.html',
        paths.dist + 'images/',
        paths.dist + 'fonts/',
        paths.dist + 'media/'
      ]
    },
    backend: {
      src: [
        paths.backend + 'css/style.css',
        paths.backend + 'js/scripts.js',
        paths.backend + 'images/**/*.{svg,png,jpg,jpeg,gif}',
        paths.backend + 'favicons/**/*.{ico,png,svg,xml,json}',
        paths.backend + 'fonts/*.{woff,woff2,eot,otf,ttf,svg}'
      ]
    }
  },
  duplicate: {
    dist: {
      fonts: {
        src: paths.assets + 'fonts/*.{woff,woff2,eot,otf,ttf,svg}',
        dest: paths.dist + 'fonts/'
      },
      images: {
        src: paths.assets + 'images/**/*.{svg,png,jpg,jpeg,gif}',
        dest: paths.dist + 'images/'
      },
      favicons: {
        src: paths.assets + 'favicons/**/*.{ico,png,svg,xml,json}',
        dest: paths.dist + 'favicons/'
      },
      media: {
        src: paths.assets + 'media/**/*.{svg,png,jpg,jpeg,gif,pdf,docx}',
        dest: paths.dist + 'media/'
      }
    },
    backend: {
      styles: {
        src: paths.dist + 'css/*.css',
        dest: paths.backend + 'css/'
      },
      scripts: {
        src: paths.dist + 'js/**/*.js',
        dest: paths.backend + 'js/'
      },
      fonts: {
        src: paths.dist + 'fonts/*.{woff,woff2,eot,otf,ttf,svg}',
        dest: paths.backend + 'fonts/'
      },
      images: {
        src: paths.dist + 'images/**/*.{svg,png,jpg,jpeg,gif}',
        dest: paths.backend + 'images/'
      },
      favicons: {
        src: paths.dist + 'favicons/**/*.{ico,png,svg,xml,json}',
        dest: paths.backend + 'favicons/'
      }
    }
  },
  uglify: {
    src: paths.dist + 'js/*.js',
    dest: paths.backend + 'js/'
  },
  postcss: {
    src: paths.dist + 'css/*.css',
    dest: paths.backend + 'css/'
  }
}
