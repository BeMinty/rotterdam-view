const gulp = require('gulp'),
      config = require('../config').duplicate.backend.favicons;

gulp.task('copy:favicons-backend', () => {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
