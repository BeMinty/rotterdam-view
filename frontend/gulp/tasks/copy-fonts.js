const gulp = require('gulp'),
      config = require('../config').duplicate.dist.fonts;

gulp.task('copy:fonts', () => {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
