const gulp = require('gulp'),
      runSequence = require('run-sequence');

gulp.task('build:backend', (callback) => {
  runSequence(
    'delete:backend',
    'optimize:css',
    'optimize:js',
    [
      'copy:fonts-backend',
      'copy:images-backend',
      'copy:favicons-backend'
    ],
    callback
  );
});
