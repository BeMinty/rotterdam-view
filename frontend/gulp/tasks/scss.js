const gulp = require('gulp'),
      scss = require('gulp-sass'),
      browserSync = require('browser-sync'),
      plumber = require('gulp-plumber'),
      autoprefixer = require('gulp-autoprefixer'),
      gmediaq = require('gulp-group-css-media-queries'),
      notify = require('gulp-notify'),
      config = require('../config');

//Custom plumber function for catching errors
function customPlumber(errTitle) {
  return plumber({
    errorHandler: (err) => {
      notify.onError({
        title: errTitle || 'Error running Gulp',
        message: err.toString()
      })(err);
    }
  });
}

gulp.task('scss', () => {
  var scssConfig = config.scss.options;
  scssConfig.onError = browserSync.notify;

  return gulp.src(config.scss.src)
    .pipe(customPlumber("Error running Sass"))
    .pipe(scss(scssConfig).on('error', scss.logError))
    .pipe(autoprefixer(config.autoprefixer.options))
    .pipe(gmediaq())
    .pipe(gulp.dest(config.scss.dest))
    .pipe(browserSync.stream())
    .pipe(notify(config.notification.scss));
});
