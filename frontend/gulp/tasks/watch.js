const gulp = require('gulp'),
      watch = require('gulp-watch'),
      config = require('../config');

gulp.task('watch', ['browser-sync'], function() {
  gulp.watch(config.scss.src, ['scss']);
  gulp.watch(config.nunjucks.paths, ['nunjucks']);
  gulp.watch(config.babel.src, ['babel-js']);
  gulp.watch(config.concatenate.js.src, ['concatenate:js']);
  gulp.watch(config.duplicate.dist.images.src, ['copy:images']);
  gulp.watch(config.duplicate.dist.media.src, ['copy:media']);
  gulp.watch(config.duplicate.dist.fonts.src, ['copy:fonts']);
  gulp.watch(config.duplicate.dist.favicons.src, ['copy:favicons']);
});
