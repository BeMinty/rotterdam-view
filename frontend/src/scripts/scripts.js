/* fade in-out background navigatie on scroll */
$(window).scroll(function() {
  var scroll = $(window).scrollTop();
  if (scroll >= 100) {
    $('.navbar').addClass('bgDown');
    $('.navbar').removeClass('bgUp');
  } else {
    {
      $('.navbar').addClass('bgUp');
      $('.navbar').removeClass('bgDown')
    }
  }
});

/* fade in-out background navigatie on click for mobile */
$('body').on('click', '.navbar-toggler', function() {
  if ($('.navbar-collapse').hasClass('show')) {
    $('.navbar').addClass('bgUp');
    $('.navbar').removeClass('bgDown');
  } else {
    {
      $('.navbar').addClass('bgDown');
      $('.navbar').removeClass('bgUp')
    }
  }
});

/* slideshow settings */

$('.slideshow').slick({
  autoplay: true,
  infinite: true,
  dots: true,
  ease:'ease',
  prevArrow:"<button type='button' class='slide-arrow prev-arrow'><i class='fa fa-chevron-left' aria-hidden='true'></i></button>",
  nextArrow:"<button type='button' class='slide-arrow next-arrow'><i class='fa fa-chevron-right' aria-hidden='true'></i></button>",
  speed: 400,
  responsive: [{
      breakpoint: 5000,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
        arrows: true,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
      }
    }
  ]
});

/* slide out contact beschikbaarheid form from the left*/
$('body').on('click', '#beschikbaar-btn', function() {
  $('#form-beschikbaarheid').removeClass('slide-in');
  $('#form-beschikbaarheid').addClass('slide-out')
});
/* slide in contact beschikbaarheid form to the left*/
$('body').on('click', '#close-btn', function() {
  $('#form-beschikbaarheid').removeClass('slide-out');
  $('#form-beschikbaarheid').addClass('slide-in')
});

/* fontawesome nesting */
FontAwesomeConfig = {
  autoReplaceSvg: 'nest'
}
