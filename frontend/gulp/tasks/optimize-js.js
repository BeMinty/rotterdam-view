const gulp = require('gulp'),
      uglify = require('gulp-uglify'),
      config = require('../config').uglify;

gulp.task('optimize:js', () => {
  return gulp.src(config.src)
    .pipe(uglify())
    .pipe(gulp.dest(config.dest));
});
