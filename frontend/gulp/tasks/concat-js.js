const gulp = require('gulp'),
      browserSync = require('browser-sync'),
      concat = require('gulp-concat'),
      plumber = require('gulp-plumber'),
      notify = require('gulp-notify'),
      config = require('../config');

//Custom plumber function for catching errors
function customPlumber(errTitle) {
  return plumber({
    errorHandler: (err) => {
      notify.onError({
        title: errTitle || 'Error running Gulp',
        message: err.toString()
      })(err);
    }
  });
}

gulp.task('concatenate:js', () => {
  return gulp.src(config.concatenate.js.src)
    .pipe(concat(config.concatenate.js.filename))
    .pipe(gulp.dest(config.concatenate.js.dest))
    .pipe(browserSync.stream())
    .pipe(notify(config.notification.concat));
});
