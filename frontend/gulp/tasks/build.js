const gulp = require('gulp'),
      runSequence = require('run-sequence');

gulp.task('build', (callback) => {
  runSequence(
    'delete:dist',
    [
      'scss',
      'nunjucks',
      'babel-js'
    ],
    'concatenate:js',
    [
      'copy:images',
      'copy:media',
      'copy:fonts',
      'copy:favicons'
    ],
    callback
  );
});
