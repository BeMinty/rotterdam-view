const gulp = require('gulp'),
      nunjucksRender = require('gulp-nunjucks-render'),
      removeEmptyLines = require('gulp-remove-empty-lines'),
      prettyHtml = require('gulp-pretty-html'),
      plumber = require('gulp-plumber'),
      notify = require('gulp-notify'),
      browserSync = require('browser-sync'),
      config = require('../config');

//Custom plumber function for catching errors
function customPlumber(errTitle) {
  return plumber({
    errorHandler: (err) => {
      notify.onError({
        title: errTitle || 'Error running Gulp',
        message: err.toString()
      })(err);
    }
  });
}

gulp.task('nunjucks', () => {
  var nunjucksConfig = config.nunjucks.options;
  nunjucksConfig.onError = browserSync.notify;

  return gulp.src(config.nunjucks.src)
    .pipe(customPlumber('Error running Nunjucks'))
    .pipe(nunjucksRender(config.nunjucks.options))
    .pipe(removeEmptyLines())
    .pipe(prettyHtml(config.html.options))
    .pipe(gulp.dest(config.nunjucks.dest))
    .pipe(browserSync.stream())
    .pipe(notify(config.notification.nunjucks));
});
