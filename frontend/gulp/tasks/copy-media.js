const gulp = require('gulp'),
      config = require('../config').duplicate.dist.media;

gulp.task('copy:media', () => {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
