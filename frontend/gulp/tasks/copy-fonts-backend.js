const gulp = require('gulp'),
      config = require('../config').duplicate.backend.fonts;

gulp.task('copy:fonts-backend', () => {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
