const gulp = require('gulp'),
      config = require('../config').duplicate.dist.favicons;

gulp.task('copy:favicons', () => {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
