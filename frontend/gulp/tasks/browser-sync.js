const gulp = require('gulp'),
      browserSync = require('browser-sync').create(),
      config = require('../config').browsersync;

gulp.task('browser-sync', ['build'], () => {
  browserSync.init(config);
});
