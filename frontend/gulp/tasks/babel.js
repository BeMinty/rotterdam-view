const gulp = require('gulp'),
      babel = require('gulp-babel'),
      plumber = require('gulp-plumber'),
      notify = require('gulp-notify'),
      config = require('../config').babel;

//Custom plumber function for catching errors
function customPlumber(errTitle) {
  return plumber({
    errorHandler: (err) => {
      notify.onError({
        title: errTitle || 'Error running Gulp',
        message: err.toString()
      })(err);
    }
  });
}

gulp.task('babel-js', () => {
  return gulp.src(config.src)
    .pipe(customPlumber("Error running babel"))
    .pipe(babel())
    .pipe(gulp.dest(config.dest));
});
