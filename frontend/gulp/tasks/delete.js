const gulp = require('gulp'),
      del = require('del'),
      config = require('../config').delete_it.dist;

gulp.task('delete:dist', () => {
  del(config.src);
});
