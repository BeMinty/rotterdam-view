const gulp = require('gulp'),
      config = require('../config').duplicate.backend.images;

gulp.task('copy:images-backend', () => {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
