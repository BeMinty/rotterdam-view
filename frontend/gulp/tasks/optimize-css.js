const gulp = require('gulp'),
      postcss = require('gulp-postcss'),
      stripcsscomments = require('gulp-strip-css-comments'),
      cssnano = require('cssnano'),
      config = require('../config').postcss;

gulp.task('optimize:css', () => {
  var plugins = [
    cssnano()
  ];

  return gulp.src(config.src)
    .pipe(postcss(plugins))
    .pipe(stripcsscomments({preserve: false}))
    .pipe(gulp.dest(config.dest));
});
