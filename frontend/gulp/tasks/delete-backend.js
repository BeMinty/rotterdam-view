const gulp = require('gulp'),
      del = require('del'),
      config = require('../config').delete_it.backend;

gulp.task('delete:backend', () => {
  del(config.src);
});
